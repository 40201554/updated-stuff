  #version 440

uniform sampler2D tex;

uniform sampler2D dudvMap;

uniform float moveFactor;

layout (location = 0) in vec2 tex_coord_out;

layout (location = 0) out vec4 colour;

void main()
{
  vec2 distortedTexCoords = texture(dudvMap, vec2(tex_coord_out.x +moveFactor, tex_coord_out.y)).rg*0.5;
  distortedTexCoords = tex_coord_out + vec2(-distortedTexCoords.x, distortedTexCoords.y+moveFactor);
  vec2 totalDistortion = (texture(dudvMap, distortedTexCoords).rg * 2.0 - 1.0) * 0.01f;
  vec2 tex_coord =tex_coord_out + totalDistortion;
  colour = texture(tex,tex_coord);
}