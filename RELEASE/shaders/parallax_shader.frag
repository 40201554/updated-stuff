#version 440 core

// This shader requires direction.frag, point.frag 

// Directional light structure
#ifndef DIRECTIONAL_LIGHT
#define DIRECTIONAL_LIGHT
struct directional_light {
  vec4 ambient_intensity;
  vec4 light_colour;
  vec3 light_dir;
};
#endif

// Point light information
#ifndef SPOT_LIGHT
#define SPOT_LIGHT
struct spot_light
{
	vec4 light_colour;
	vec3 position;
	vec3 direction;
	float constant;
	float linear;
	float quadratic;
	float power;
};
#endif


// A material structure
#ifndef MATERIAL
#define MATERIAL
struct material {
  vec4 emissive;
  vec4 diffuse_reflection;
  vec4 specular_reflection;
  float shininess;
};
#endif
vec4 calculate_spot(in spot_light spot, in material mat, in vec3 position, in vec3 normal, in vec3 view_dir, in vec4 tex_colour,int cell);
// Forward declarations of used functions
vec4 calculate_direction(in directional_light light, in material mat, in vec3 normal, in vec3 view_dir,
                         in vec4 tex_colour, int cell);

vec2 ParallaxMapping(vec2 texCoords , vec3 viewDir);

// Directional light information
uniform directional_light light;
// Point lights being used in the scene
uniform spot_light spots[8];
// Material of the object being rendered
uniform material mat;
// Position of the eye
// uniform vec3 eye_pos; - vertex shader - view pos
// Texture to sample from
uniform sampler2D tex;

uniform sampler2D normal_map;

uniform sampler2D depth_map;

uniform float height_scale;

uniform int cell;
// Incoming position
layout (location = 0) in vec3 position;
// Outgoing transformed normal

// Outgoing texture coordinate
layout (location = 1) in vec2 tex_coord_out;

layout (location = 2) in vec3 tangent_light;
// Outgoing tangent
layout(location = 3) in vec3 tangent_view;
// Outgoing binormal
layout(location = 4) in vec3 tangent_frag;

// Outgoing colour
layout(location = 0) out vec4 colour;

void main() {

  // Calculate view direction
  vec3 view_dir = normalize (tangent_view-tangent_frag);
  // Sample texture
 
  vec2 tex_coords = ParallaxMapping(tex_coord_out,view_dir);
  //vec2 tex_coords = tex_coord_out;

  vec4 tex_colour = texture(tex,tex_coords);
  vec4 normal_m = texture(normal_map,tex_coords);
  normal_m = normalize(normal_m * 2.0 -1.0);


  //Lightning : 
  colour = calculate_direction(light,mat,normal_m.xyz,view_dir,tex_colour,cell);
  // Sum spot lights
  for (int i = 0 ; i<7 ; i++)
	{
		colour += calculate_spot(spots[i],mat,position,normal_m.xyz,view_dir,tex_colour,cell);
	}			


	float gamma = 2.2;
	colour.rgb = pow(colour.rgb,vec3(1/gamma));


}