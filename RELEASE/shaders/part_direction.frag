// Directional light structure
#ifndef DIRECTIONAL_LIGHT
#define DIRECTIONAL_LIGHT
struct directional_light
{
	vec4 ambient_intensity;
	vec4 light_colour;
	vec3 light_dir;
};
#endif

// A material structure
#ifndef MATERIAL
#define MATERIAL
struct material
{
	vec4 emissive;
	vec4 diffuse_reflection;
	vec4 specular_reflection;
	float shininess;
};

float step (float edge , float x)
{
	return x<edge ? 0.0 : 1.0;
}
float smoothstep (float edge0,float edge1,float x)
{
	if (x<=edge0)
	return 0;
	if(x>= edge1)
	return 1.0;

	float t  = clamp((x-edge0) /(edge1-edge0),0,1); 
	return t*t*(3-2*t);
}

float mixF (float a, float b , float t)
{
	return a*(1.0 - t) + b * t;
}
#endif
// Calculates the directional light
vec4 calculate_direction(in directional_light light, in material mat, in vec3 normal, in vec3 view_dir, in vec4 tex_colour ,int cell)
{
 // *********************************
 	const float A = 0.1;
	const float B = 0.3;
	const float C = 0.6;
	const float D = 0.9;
	vec3 L = light.light_dir;

	// Calculate ambient component
    vec4 ambient = mat.diffuse_reflection*light.ambient_intensity;
	// Calculate diffuse component :  (diffuse reflection * light_colour) *  max(dot(normal, light direction), 0)
	 float df = max(dot(normal,L),0.0f);
	 float E;
	 if(cell == 1)
	 {
		 E = fwidth(df);

		if (df > A - E && df < A + E)
			df = mix(A, B, smoothstep(A - E, A + E, df));
		 else if (df > B - E && df < B + E)
			df = mix(B, C, smoothstep(B - E, B + E, df));
		  else if (df > C - E && df < C + E)
		  df = mix(C, D, smoothstep(C - E, C + E, df));
		  else if(df<A) 
			df =0.0f;
			else if (df<B)
			df = B;
			else if(df<C)
		   df = C;
			else 
			df = D;
	}
    vec4 diffuse = mat.diffuse_reflection*light.light_colour * df;
	// Calculate normalized half vector 
    vec3 half_vector = normalize(view_dir + light.light_dir);
	// Calculate specular component : (specular reflection * light_colour) * (max(dot(normal, half vector), 0))^mat.shininess
	 float sf = pow(max(dot(normal,half_vector),0),mat.shininess);
	 E = fwidth(sf) ; 
 if (cell == 1)
 {
		if(sf > 0.5 -E && sf< 0.5 + E)
		{
			sf = clamp(0.5 * (sf - 0.5 + E)/E , 0.0,1.0);
		}
		else
			sf = step(0.5,sf);
}
    vec4 specular = mat.specular_reflection * light.light_colour * sf;
 // *********************************
	// Calculate colour to return
	vec4 colour1 = ((mat.emissive + ambient + diffuse) * tex_colour) + specular;
	colour1.a = 1.0;
	// Return colour
	return colour1;

}
