// Spot light data
#ifndef SPOT_LIGHT
#define SPOT_LIGHT
struct spot_light
{
	vec4 light_colour;
	vec3 position;
	vec3 direction;
	float constant;
	float linear;
	float quadratic;
	float power;
};
#endif

// Material data
#ifndef MATERIAL
#define MATERIAL
struct material
{
	vec4 emissive;
	vec4 diffuse_reflection;
	vec4 specular_reflection;
	float shininess;
};
#endif
float step (float edge , float x)
{
	return x<edge ? 0.0 : 1.0;
}
float smoothstep (float edge0,float edge1,float x)
{
	if (x<=edge0)
	return 0;
	if(x>= edge1)
	return 1.0;

	float t  = clamp((x-edge0) /(edge1-edge0),0,1); 
	return t*t*(3-2*t);
}

float mixF (float a, float b , float t)
{
	return a*(1.0 - t) + b * t;
}
// Spot light calculation
vec4 calculate_spot(in spot_light spot, in material mat, in vec3 position, in vec3 normal, in vec3 view_dir, in vec4 tex_colour,int cell)
{
	const float A = 0.1;
	const float B = 0.3;
	const float C = 0.6;
	const float D = 0.9;
	// *********************************
	vec4 colour1 = vec4(0,0,0,1);
	// Calculate direction to the light
    vec3 L = normalize(spot.position - position);
	// Calculate distance to light
    float d = distance(spot.position,position);
	// Calculate attenuation value :  (constant + (linear * d) + (quadratic * d * d)
    float att = spot.constant + spot.linear * d + spot.quadratic*d*d; 
	// Calculate spot light intensity :  (max( dot(light_dir, -direction), 0))^power
	float intensity = pow(max(dot(L,-spot.direction),0),spot.power);
	// Calculate light colour:  (intensity / attenuation) * light_colour
    vec4 light_colour = (intensity / att)*spot.light_colour;
	// *********************************
	// Now use standard phong shading but using calculated light colour and direction
	 float df = max(dot(normal,L),0.0f);
	 float E;
	 if(cell == 1)
	 {
		 E = fwidth(df);

		if (df > A - E && df < A + E)
			df = mix(A, B, smoothstep(A - E, A + E, df));
		 else if (df > B - E && df < B + E)
			df = mix(B, C, smoothstep(B - E, B + E, df));
		  else if (df > C - E && df < C + E)
		  df = mix(C, D, smoothstep(C - E, C + E, df));
		  else if(df<A) 
			df =0.0f;
			else if (df<B)
			df = B;
			else if(df<C)
		   df = C;
			else 
			df = D;
	}
	vec4 diffuse = (mat.diffuse_reflection * light_colour) * df;
	vec3 half_vector = normalize(L + view_dir);
	 float sf = pow(max(dot(normal,half_vector),0),mat.shininess);

 E = fwidth(sf) ; 
 if (cell == 1)
 {
		if(sf > 0.5 -E && sf< 0.5 + E)
		{
			sf = clamp(0.5 * (sf - 0.5 + E)/E , 0.0,1.0);
		}
		else
			sf = step(0.5,sf);
}
	vec4 specular = (mat.specular_reflection * light_colour) * sf;
	
	colour1 = ((mat.emissive + diffuse) * tex_colour) + specular;
	colour1.a = 1.0;

	return colour1;
}