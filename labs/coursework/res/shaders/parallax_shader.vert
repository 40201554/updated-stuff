#version 440 core

// Model transformation matrix
uniform mat4 M;
// Transformation matrix
uniform mat4 MVP;

uniform vec3 lightPos;
uniform vec3 viewPos;

// Incoming position
layout (location = 0) in vec3 position;
// Incoming normal
layout (location = 2) in vec3 normal;
// Incoming texture coordinate
layout(location = 4) in vec3 bitangent;
// Incoming tangent
layout(location = 3) in vec3 tangent;

layout (location = 10) in vec2 tex_coord_in;

// Outgoing position
layout (location = 0) out vec3 vertex_position;
// Outgoing transformed normal
layout (location = 2) out vec3 tangent_light;
// Outgoing texture coordinate
layout (location = 1) out vec2 tex_coord_out;
// Outgoing tangent
layout(location = 3) out vec3 tangent_view;
// Outgoing binormal
layout(location = 4) out vec3 tangent_frag;

void main()
{
  // Calculate screen position
  gl_Position = MVP * vec4(position, 1.0);
  
  vertex_position = vec3 (M * vec4(position,1.0));

  tex_coord_out = tex_coord_in;
    
	// Transform tangent
	vec3 T   = normalize(mat3(M)*tangent);
	vec3 B   = normalize(mat3(M)*bitangent);
	vec3 N   = normalize(mat3(M)*normal);
	mat3 TBN = transpose(mat3(T, B, N));

	tangent_light = TBN * lightPos;
	tangent_view = TBN * viewPos;
	tangent_frag = TBN * vertex_position;
  // *********************************
}