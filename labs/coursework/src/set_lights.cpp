#include <glm\glm.hpp>
#include <graphics_framework.h>
#include "set_lights.h"
#include "main.h"

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;

void setMaterials()
{
	material mat;

	mat.set_emissive(vec4(1.0f, 0.5f, 0.0f, 1.0f));
	mat.set_shininess(25.0f);
	mat.set_specular(vec4(1.0f, 1.0f, 1.0f, 1.0f));
	mat.set_diffuse(vec4(0.3f, 0.2f, 0.0f, 1.0f));
	meshes["torusOrange"].set_material(mat);
	meshes["torusOrange"].get_transform().scale *= vec3(0.5f, 0.5f, 1.0f);
	mat.set_diffuse(vec4(0, 0, 0.5, 1));
	mat.set_emissive(vec4(1.0f, 0.0f, 0.0f, 1.0f));
	meshes["torusBlue"].set_material(mat);
	//mat.set_diffuse(vec4(0.32, 0.178, 0.170, 1));
	mat.set_diffuse(vec4(1, 0, 0, 1));
	meshes["torusBlue"].get_transform().scale *= vec3(0.5f, 0.5f, 1.0f);

	mat.set_diffuse(vec4(0, 0, 0, 1));
	mat.set_emissive(vec4(1, 0, 0.2, 1));
	hierarchy_meshes[0].set_material(mat);

	mat.set_emissive(vec4(1, 0, 0, 1));
	hierarchy_meshes[1].set_material(mat);

	mat.set_emissive(vec4(0.6, 0.4, 0, 1));
	hierarchy_meshes[2].set_material(mat);

	mat.set_emissive(vec4(0, 0.1, 1, 1));
	hierarchy_meshes[3].set_material(mat);

	mat.set_diffuse(vec4(1, 1, 1, 1));
	mat.set_emissive(vec4(0, 0, 0, 1));

}
void setLights()
{
	// Set lighting values

	spots[0].set_position(vec3(-25.0f, 7.0f, -20.0f));
	spots[0].set_light_colour(vec4(0, 0, 0, 1));
	spots[0].set_range(15);
	spots[0].set_power(0.01f);
	spots[0].set_direction(vec3(0, 0, -1));


	spots[1].set_position(vec3(50.0f, 10.0f, 0.0f));
	spots[1].set_light_colour(vec4(0, 0, 0, 1));
	spots[1].set_range(100);
	spots[1].set_power(1.0f);
	spots[1].set_direction(normalize(vec3(0, 0, 1)));

	spots[2].set_position(vec3(-10, 10, -15));
	spots[2].set_light_colour(vec4(0, 0, 1, 1));
	spots[2].set_range(15);
	spots[2].set_power(0.01f);
	spots[2].set_direction(vec3(1, 0, 1));

	spots[3].set_position(vec3(-10, 10, -15));
	spots[3].set_light_colour(vec4(1, 0, 0, 1));
	spots[3].set_range(15);
	spots[3].set_power(0.01f);
	spots[3].set_direction(vec3(-1, 0, 1));

	spots[4].set_position(vec3(-10, 10, -15));
	spots[4].set_light_colour(vec4(0, 1, 1, 1));
	spots[4].set_range(15);
	spots[4].set_power(0.5f);
	spots[4].set_direction(vec3(1, 0, -1));

	spots[5].set_position(vec3(-10, 10, -15));
	spots[5].set_light_colour(vec4(1, 1, 0, 1));
	spots[5].set_range(15);
	spots[5].set_power(0.01f);
	spots[5].set_direction(vec3(-1, 0, -1));

	spots[6].set_position(vec3(23, 25, -15));
	spots[6].set_light_colour(vec4(1, 1, 1, 1));
	spots[6].set_range(15);
	spots[6].set_power(1.0f);
	spots[6].set_direction(vec3(0, -1, 0));

	spots[7].set_position(vec3(23, 23, 15));
	spots[7].set_light_colour(vec4(1, 1, 1, 1));
	spots[7].set_range(15);
	spots[7].set_power(0.01f);
	spots[7].set_direction(vec3(0, -1, 0));
}