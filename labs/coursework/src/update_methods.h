#pragma once
#include <glm\glm.hpp>
#include <graphics_framework.h>

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;

void update_moving_meshes(float delta_time);
void update_cameras(float delta_time);
void update_mouse_position(float delta_time);
bool checkTorusRadius(vec3 c, vec3 m, float radius);
