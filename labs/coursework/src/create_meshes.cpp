#include <glm\glm.hpp>
#include <graphics_framework.h>
#include "create_meshes.h"
#include "main.h"

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;

void createRegularMeshes()
{
	parallax_meshes["plane1"] = mesh(geometry_builder::create_plane(200, 200, true));
	parallax_meshes["plane1"].get_transform().scale *= vec3(0.5, 1, 0.5);

	meshes["pc"] = mesh(geometry("models/pc.obj"));
	meshes["pc"].get_transform().scale *= 0.5;
	meshes["pc"].get_transform().rotate(vec3(0, -half_pi<float>(), 0));
	meshes["pc"].get_transform().translate(vec3(17, 4, 0));

	meshes["pcTable"] = mesh(geometry_builder::create_box(vec3(6, 4, 6)));
	//meshes["pcTable"].get_transform().scale *= 0.00001f;
	meshes["pcTable"].get_transform().rotate(vec3(0, -half_pi<float>(), 0));
	meshes["pcTable"].get_transform().translate(vec3(16, 2, 0));

	meshes["wolf"] = mesh(geometry("models/Wolf.obj"));
	meshes["wolf"].get_transform().scale *= 0.05;
	meshes["wolf"].get_transform().translate(vec3(55, 8, 20));
	meshes["wolf"].get_transform().rotate(vec3(0, 3.14, 0));

	parallax_meshes["speaker1"] = mesh(geometry("models/Speaker.obj"));
	parallax_meshes["speaker1"].get_transform().rotate(vec3(0, -half_pi<float>(), 0));
	parallax_meshes["speaker1"].get_transform().translate(vec3(23.5, 12.8, 0));
	parallax_meshes["speaker1"].get_transform().scale *= 5;

	parallax_meshes["speaker2"] = mesh(geometry("models/Speaker.obj"));
	parallax_meshes["speaker2"].get_transform().rotate(vec3(0, -quarter_pi<float>(), 0));
	parallax_meshes["speaker2"].get_transform().translate(vec3(23.5, 14.8, -21));
	parallax_meshes["speaker2"].get_transform().scale *= 5;

	parallax_meshes["speaker3"] = mesh(geometry("models/Speaker.obj"));
	parallax_meshes["speaker3"].get_transform().rotate(vec3(0, -3 * quarter_pi<float>(), 0));
	parallax_meshes["speaker3"].get_transform().translate(vec3(23.5, 16.8, 21));
	parallax_meshes["speaker3"].get_transform().scale *= 5;
	//Portals
	meshes["torusBlue"] = mesh(geometry_builder::create_torus(20, 20, 0.75f, 5.0f));
	meshes["torusOrange"] = mesh(geometry_builder::create_torus(20, 20, 0.75f, 5.0f));
	meshes["torusBlue"].get_transform().translate(vec3(-25.0f, 7.0f, -25.0f));
	meshes["torusBlue"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));
	meshes["torusOrange"].get_transform().translate(vec3(55.0f, 7.0f, -20.0f));
	meshes["torusOrange"].get_transform().rotate(vec3(half_pi<float>(), 0.0f, 0.0f));

	meshes["diskOrange"] = mesh(geometry_builder::create_disk(20));
	meshes["diskOrange"].get_transform().position = meshes["torusOrange"].get_transform().position;
	meshes["diskOrange"].get_transform().rotate(vec3(half_pi<float>(), 0, 0));
	meshes["diskOrange"].get_transform().scale *= vec3(5, 0, 10);

	meshes["diskBlue"] = mesh(geometry_builder::create_disk(20));
	meshes["diskBlue"].get_transform().position = meshes["torusBlue"].get_transform().position;
	meshes["diskBlue"].get_transform().rotate(vec3(half_pi<float>(), 0, 0));
	meshes["diskBlue"].get_transform().scale *= vec3(5, 0, 10);

	meshes["speakerStand1"] = mesh(geometry_builder::create_box(vec3(6, 10, 6)));
	meshes["speakerStand1"].get_transform().translate(vec3(22, 5, 0));

	meshes["speakerStand2"] = mesh(geometry_builder::create_box(vec3(6, 12, 6)));
	meshes["speakerStand2"].get_transform().translate(vec3(22, 6, -20));
	meshes["speakerStand2"].get_transform().rotate(vec3(0, quarter_pi<float>(), 0));

	meshes["speakerStand3"] = mesh(geometry_builder::create_cylinder((10, 14, 10)));
	meshes["speakerStand3"].get_transform().scale *= vec3(10, 8, 10);
	meshes["speakerStand3"].get_transform().translate(vec3(55, 4, 20));
	meshes["speakerStand3"].get_transform().rotate(vec3(0, quarter_pi<float>(), 0));
	cams[2]->set_position(meshes["torusOrange"].get_transform().position - vec3(0, 0, -5));
	cams[3]->set_position(meshes["torusBlue"].get_transform().position);
	// *********************************
}
void createHierachicalMeshes()
{
	hierarchy_meshes[0] = mesh(geometry_builder::create_torus(20, 20, 0.5, 5.0f));
	hierarchy_meshes[0].get_transform().translate(vec3(0, 20, 0));

	hierarchy_meshes[1] = mesh(geometry_builder::create_torus(20, 20, 0.5, 4.0f));
	//hierarchy_meshes[1].get_transform().translate(vec3(0, 7, 0));

	hierarchy_meshes[2] = mesh(geometry_builder::create_torus(20, 20, 0.5, 3.0f));
	//hierarchy_meshes[2].get_transform().translate(vec3(0, 7, 0));

	hierarchy_meshes[3] = mesh(geometry_builder::create_torus(20, 20, 0.5, 2.0f));
}
void particle_load()
{
	default_random_engine rand(duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count());
	uniform_real_distribution<float> dist;

	// Initilise particles
	for (unsigned int i = 0; i < MAX_PARTICLES; ++i) {
		particles[i].position = vec3(((dist(rand)) * 50 - 30), 25.0f, dist(rand) * 120 - 80);
		particles[i].velocity = vec3(-0.1f - dist(rand) - 0.1, -0.1f - dist(rand) * 10 - 5, 0.0f);
	}
	// Use the particle effect
	renderer::bind(particle_eff);
	// Tell OpenGL what the output looks like
	const GLchar *attrib_names[2] = { "position_out", "velocity_out" };
	glTransformFeedbackVaryings(particle_eff.get_program(), 2, attrib_names, GL_INTERLEAVED_ATTRIBS);

	// Relink program
	glLinkProgram(particle_eff.get_program());

	// a useless vao, but we need it bound or we get errors.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Gen our VBO data buffers
	glGenBuffers(2, particle_buffers_vbo);
	// *********************************
	// Place initial particle data in buffer 1
	glBindBuffer(GL_ARRAY_BUFFER, particle_buffers_vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(particle) * MAX_PARTICLES, particles, GL_DYNAMIC_DRAW);
	// Fill space with blank data in buffer 2
	glBindBuffer(GL_ARRAY_BUFFER, particle_buffers_vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(particle) * MAX_PARTICLES, 0, GL_DYNAMIC_DRAW);


	// generate our feedback objects
	glGenTransformFeedbacks(2, transform_feedbacks);
	// link fb[0] to vbo[1]
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transform_feedbacks[0]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, particle_buffers_vbo[1]);
	// *********************************
	// link fb[1] to vbo[0]
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transform_feedbacks[1]);
	glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, particle_buffers_vbo[0]);
}
void create_effects()
{
	particle_eff.add_shader("shaders/particle.vert", GL_VERTEX_SHADER);
	particle_eff.add_shader("shaders/particle.geom", GL_GEOMETRY_SHADER);
	particle_eff.add_shader("shaders/particle.frag", GL_FRAGMENT_SHADER);
	particle_eff.build();

	distortion_eff.add_shader("shaders/edge.vert", GL_VERTEX_SHADER);
	distortion_eff.add_shader("shaders/distortion.frag", GL_FRAGMENT_SHADER);
	distortion_eff.build();

	mask_eff.add_shader("shaders/edge.vert", GL_VERTEX_SHADER);
	mask_eff.add_shader("shaders/mask.frag", GL_FRAGMENT_SHADER);
	mask_eff.build();

	edge_eff.add_shader("shaders/edge.vert", GL_VERTEX_SHADER);
	edge_eff.add_shader("shaders/edge.frag", GL_FRAGMENT_SHADER);
	edge_eff.build();

	particle_load();
	//Texture effect   
	tex_eff.add_shader("shaders/edge.vert", GL_VERTEX_SHADER);
	tex_eff.add_shader("shaders/simple_texture.frag", GL_FRAGMENT_SHADER);
	tex_eff.build();
	// Main effect
	eff.add_shader("shaders/shader.vert", GL_VERTEX_SHADER);
	vector<string> frag_shaders{ "shaders/shader.frag", "shaders/part_direction.frag",
		"shaders/part_spot.frag","shaders/part_normal_map.frag" };
	eff.add_shader(frag_shaders, GL_FRAGMENT_SHADER);
	eff.build();
	//Parallax effect
	parallax_eff.add_shader("shaders/parallax_shader.vert", GL_VERTEX_SHADER);
	vector<string> pfrag_shaders{ "shaders/parallax_shader.frag", "shaders/part_direction.frag",
		"shaders/part_spot.frag","shaders/parralax_function.frag" };
	parallax_eff.add_shader(pfrag_shaders, GL_FRAGMENT_SHADER);
	parallax_eff.build();

	//Skybox effect
	sky_eff.add_shader("shaders/skybox.vert", GL_VERTEX_SHADER);
	sky_eff.add_shader("shaders/skybox.frag", GL_FRAGMENT_SHADER);
	sky_eff.build();
}