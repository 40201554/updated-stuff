#pragma once
#include <glm\glm.hpp>
#include <graphics_framework.h>

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;
// Maximum number of particles  
extern const unsigned int MAX_PARTICLES ;

// A particle
extern struct particle {
	vec3 position;
	vec3 velocity;
};
extern bool normal ;
// Particles in the system
extern particle particles[3000];
// The transform feedback buffers
extern GLuint transform_feedbacks[2];
// The particle buffers
extern GLuint particle_buffers_vbo[2];
extern GLuint vao;
// Effects
extern effect particle_eff;
// Current buffer to perform the physics update to
extern unsigned int front_buf ;
// Buffer to render to
extern unsigned int back_buf ;

extern array<camera*, 4> cams;
extern int cameraIndex;
extern map<string, mesh> meshes;
extern map<string, mesh> parallax_meshes;
extern array<mesh, 4> hierarchy_meshes;

extern effect tex_eff;
extern effect eff;
extern effect parallax_eff;
extern effect mask_eff;
extern effect distortion_eff;
extern effect sky_eff;
extern effect edge_eff;

extern texture tex, normal_map, depth_map, dudv_map, alpha_map;

extern directional_light light;
extern vector<spot_light> spots;

extern map<string, texture> texs;
extern map<string, texture> normal_maps;
extern map<string, texture> depth_maps;

extern float height_scale ;
extern float velocity ;
extern double cursor_x ;
extern double cursor_y ;
extern float moveFactor;
extern int cell;
extern bool lightningToggle;

extern frame_buffer frame, frameVision, frameDistortion;

extern geometry screen_quad;
extern cubemap cube_map;
extern mesh skybox;
