#pragma once
#include <glm\glm.hpp>
#include <graphics_framework.h>

using namespace std;
using namespace graphics_framework;
using namespace glm;

void createRegularMeshes();
void createHierachicalMeshes();
void particle_load();
void create_effects();
