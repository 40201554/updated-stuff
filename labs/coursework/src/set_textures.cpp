#include <glm\glm.hpp>
#include <graphics_framework.h>
#include "set_textures.h"
#include "main.h"

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;

void setTextures()
{
	alpha_map = texture("textures/night_vision.jpg");
	dudv_map = texture("textures/noise.jpg");
	texs["torusOrange"] = texture("textures/fireOrange.jpg");
	texs["torusBlue"] = texture("textures/fireBlue.jpg");
	texs["plane1"] = texture("textures/brickb.jpg");

	texs["speakerStand1"] = texture("textures/silk.jpg");
	//texs["speakerStand1"] = texture(colour_data, 32, 32, true, false);
	texs["wolf"] = texture("textures/fur.jpg");
	texs["speakerStand2"] = texs["speakerStand1"];
	texs["speakerStand3"] = texture("textures/stones.jpg");
	texs["white"] = texture("textures/white.jpg");
	texs["speaker1"] = texture("textures/skyscraper.jpg");
	texs["speaker2"] = texs["speaker1"];
	texs["speaker3"] = texs["speaker1"];
	texs["pc"] = texs["speaker1"];
	texs["pcTable"] = texture("textures/silk.jpg");
	texs["diskOrange"] = texture("textures/TorusOrange.png");
	texs["diskBlue"] = texture("textures/TorusBlue.png");
	normal_maps["torusBlue"] = texture("textures/fire_normal_map.jpg");

	normal_maps["speakerStand1"] = texture("textures/fancy_map.jpg");
	normal_maps["speakerStand2"] = normal_maps["speakerStand1"];
	//normal_maps["speakerStand3"] = normal_maps["speakerStand1"];
	normal_maps["pcTable"] = normal_maps["speakerStand1"];
	normal_maps["basic"] = texture("textures/basic_map.jpg");
	normal_maps["plane1"] = texture("textures/brickn.jpg");
	normal_maps["speaker1"] = texture("textures/brickn.jpg");
	normal_maps["speaker2"] = normal_maps["speaker1"];
	normal_maps["speaker3"] = normal_maps["speaker1"];
	//depth_maps["basic"] = texture("textures/toy_box_disp.png"); 
	depth_maps["plane1"] = texture("textures/brickr.jpg");

}