#pragma once
#include <glm\glm.hpp>
#include <graphics_framework.h>

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;

void renderSkybox(camera* cam);
void renderHierarchy(camera* cam);
void renderParallax(camera* cam);
void renderMain(camera* cam);
void render_feeback();
void renderParticles(camera* cam);
void renderPortals(camera* cam, camera* cam2, int nr);
void renderEdge(camera* cam);
void renderHierarchyOptimisation();
void renderParallaxOptimisation();
void renderMainOptimisation();