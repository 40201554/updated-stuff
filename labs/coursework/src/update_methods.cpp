#include <glm\glm.hpp>
#include <graphics_framework.h>
#include "update_methods.h"
#include "main.h"

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;

bool checkTorusRadius(vec3 c, vec3 m, float radius)
{
	bool ok = false;
	if (c.x < m.x + radius && c.x >m.x - radius && c.y < m.y + radius && c.y >m.y - radius && c.z - m.z < 0.5f)
		ok = true;
	return ok;
}
void update_moving_meshes(float delta_time)
{
	spots[1].rotate(vec3(0, pi<float>(), 0)*delta_time);
	spots[2].rotate(vec3(0, pi<float>(), 0)*delta_time);
	spots[3].rotate(vec3(0, pi<float>(), 0)*delta_time);
	spots[4].rotate(vec3(0, pi<float>(), 0)*delta_time);
	hierarchy_meshes[0].get_transform().rotate(vec3(0, 0, half_pi<float>())*delta_time);
	hierarchy_meshes[1].get_transform().rotate(vec3(half_pi<float>(), half_pi<float>(), 0)*delta_time);
	hierarchy_meshes[2].get_transform().rotate(vec3(half_pi<float>(), 0, 0)*delta_time);
	hierarchy_meshes[3].get_transform().rotate(vec3(0, 0, half_pi<float>())*delta_time);
	parallax_meshes["speaker1"].get_transform().scale += sin(velocity * 10)*0.015f;
	parallax_meshes["speaker2"].get_transform().scale += sin(velocity * 10)*0.015f;
	parallax_meshes["speaker3"].get_transform().scale += sin(velocity * 10)*0.015f;
	meshes["wolf"].get_transform().rotate(vec3(0, 3.14 / 2 * delta_time, 0));
}
void update_mouse_position(float delta_time)
{
	//Mouse
	static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
	static double ratio_height =
		(quarter_pi<float>() *
		(static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) /
		static_cast<float>(renderer::get_screen_height());

	double current_x = 0;
	double current_y = 0;
	// Get the current cursor position
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);
	// Calculate delta of cursor positions from last frame
	double delta_x = current_x - cursor_x;
	double delta_y = current_y - cursor_y;
	// Multiply deltas by ratios - gets actual change in orientation
	delta_x = delta_x * ratio_width;
	delta_y = delta_y * ratio_height;
	// Rotate cameras by delta
	// delta_y - x-axis rotation
	// delta_x - y-axis rotation
	static_cast<free_camera*>(cams[cameraIndex])->rotate(delta_x, delta_y);
	glfwSetCursorPos(renderer::get_window(), current_x, current_y);
	cursor_x = current_x;
	cursor_y = current_y;
}
void update_cameras(float delta_time)
{
	if (cameraIndex == 0)
	{
		if (checkTorusRadius(cams[cameraIndex]->get_position(), meshes["torusOrange"].get_transform().position, 5))
		{
			cams[cameraIndex]->set_position(meshes["torusBlue"].get_transform().position + vec3(1, 1, 1));
			static_cast<free_camera*>(cams[cameraIndex])->rotate(pi<float>(), 0);
		}
		if (checkTorusRadius(cams[cameraIndex]->get_position(), meshes["torusBlue"].get_transform().position, 5))
		{
			cams[cameraIndex]->set_position(meshes["torusOrange"].get_transform().position + vec3(0, 0, 2));
			static_cast<free_camera*>(cams[cameraIndex])->rotate(pi<float>(), 0);
		}

		// Use keyboard to move the camera - WSAD
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_W))
		{
			static_cast<free_camera*>(cams[cameraIndex])->move(vec3(0, 0, 20)*delta_time);
		}
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_S))
		{
			static_cast<free_camera*>(cams[cameraIndex])->move(vec3(0, 0, -20)*delta_time);
		}
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_A))
		{
			static_cast<free_camera*>(cams[cameraIndex])->move(vec3(-20, 0, 0)*delta_time);
		}
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
		{
			static_cast<free_camera*>(cams[cameraIndex])->move(vec3(20, 0, 0)*delta_time);
		}

	}
	else if (cameraIndex == 1)
	{
		if (glfwGetKey(renderer::get_window(), '1')) {
			cams[cameraIndex]->set_position(vec3(50, 10, 50));
		}
		if (glfwGetKey(renderer::get_window(), '2')) {
			cams[cameraIndex]->set_position(vec3(-50, 10, 50));
		}
		if (glfwGetKey(renderer::get_window(), '3')) {
			cams[cameraIndex]->set_position(vec3(-50, 10, -50));
		}
		if (glfwGetKey(renderer::get_window(), '4')) {
			cams[cameraIndex]->set_position(vec3(50, 10, -50));
		}
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_C))
	{
		if (cameraIndex == 0)
			cameraIndex = 1;
		else if (cameraIndex == 1)
			cameraIndex = 0;
	}
	cams[cameraIndex]->update(delta_time);
	cams[2]->update(delta_time);
	cams[3]->update(delta_time);
}
