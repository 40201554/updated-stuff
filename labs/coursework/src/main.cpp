
#include <glm\glm.hpp>
#include <graphics_framework.h>
#include "create_meshes.h" 
#include "set_textures.h"
#include "set_lights.h"
#include "update_methods.h"
#include "render_meshes.h"
#include "main.h"

using namespace std; 
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm; 


const unsigned int MAX_PARTICLES = 3000;

// A particle

bool normal = true;
// Particles in the system
particle particles[3000];
// The transform feedback buffers
GLuint transform_feedbacks[2];
// The particle buffers
GLuint particle_buffers_vbo[2];
GLuint vao;
// Effects
effect particle_eff;
// Current buffer to perform the physics update to
unsigned int front_buf = 0;
// Buffer to render to
unsigned int back_buf = 1;

array<camera*, 4> cams;
int cameraIndex = 0;
map<string, mesh> meshes;
map<string, mesh> parallax_meshes;
array<mesh, 4> hierarchy_meshes;

effect tex_eff;
effect eff;
effect parallax_eff;
effect mask_eff;
effect distortion_eff;
effect sky_eff;
effect edge_eff;

texture tex, normal_map, depth_map, dudv_map, alpha_map;

directional_light light;
vector<spot_light> spots(8);

map<string, texture> texs;
map<string, texture> normal_maps;
map<string, texture> depth_maps;

float height_scale = 0.1f;
float velocity = 0;
double cursor_x = 0.0;
double cursor_y = 0.0;
float moveFactor;
int cell = 0;
bool lightningToggle = true;

frame_buffer frame, frameVision, frameDistortion;

geometry screen_quad;
cubemap cube_map;
mesh skybox;

bool initialise() {
	for (particle p : particles)
	{
		p.velocity = vec3(0, 0, 0);
		p.position = vec3(0, 0, 0);
	}
	// *********************************
	// Set input mode - hide the cursor
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	// Capture initial mouse position
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);
	// *********************************
	cams[0] = new free_camera();
	cams[1] = new target_camera();
	cams[1]->set_position(vec3(50.0f, 10.0f, 50.0f));
	cams[1]->set_target(vec3(0.0f, 0.0f, 0.0f));
	cams[1]->set_projection(quarter_pi<float>(), renderer::get_screen_aspect(), 0.1f, 1000.0f);
	cams[2] = new target_camera();
	cams[3] = new target_camera();
	cams[2]->set_target(vec3(55,8,20));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cams[2]->set_projection(quarter_pi<float>()*1.5f, aspect, 2.414f, 1000.0f);
	cams[3]->set_target(vec3(0, 0, 1));
	cams[3]->set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	return true;
}      
bool load_content() {
	frame = frame_buffer(renderer::get_screen_width(), renderer::get_screen_height());
	frameVision = frame_buffer(renderer::get_screen_width(), renderer::get_screen_height());
	frameDistortion = frame_buffer(renderer::get_screen_width(), renderer::get_screen_height());


	skybox = mesh(geometry_builder::create_box());

	vector <vec3>positions{ vec3(-1.0f, -1.0f, 0.0f), vec3(1.0f, -1.0f, 0.0f), vec3(-1.0f, 1.0f, 0.0f),
		vec3(1.0f, 1.0f, 0.0f) };
	vector<vec2> tex_coords{ vec2(0.0, 0.0), vec2(1.0f, 0.0f), vec2(0.0f, 1.0f), vec2(1.0f, 1.0f) };
	screen_quad.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
	screen_quad.add_buffer(tex_coords, BUFFER_INDEXES::TEXTURE_COORDS_0);
	screen_quad.set_type(GL_TRIANGLE_STRIP);

	// Scale box by 100
	skybox.get_transform().scale *= 1000;

	array<string, 6> filenames = { "textures/sincity_ft.png" ,"textures/sincity_bk.png","textures/sincity_up.png",
		"textures/sincity_dn.png","textures/sincity_rt.png","textures/sincity_lf.png"
	};

	createRegularMeshes();

	createHierachicalMeshes(); 
	 
	setMaterials();

	setTextures();
	       
	setLights();

	create_effects();

	cube_map = cubemap(filenames);
	//Particle effect
	
	// Set camera properties
	cams[cameraIndex]->set_position(vec3(0.0f, 10.0f, 50.0f));
	cams[cameraIndex]->set_target(vec3(0.0f, 0.0f, 0.0f));
	cams[cameraIndex]->set_projection(quarter_pi<float>(), renderer::get_screen_aspect(), 0.1f, 1000.0f);
	skybox.get_transform().position = cams[cameraIndex]->get_position();
	return true;
} 
bool update(float delta_time) {	
	//Particle
	update_moving_meshes(delta_time);
	update_mouse_position(delta_time);
	moveFactor += delta_time/2.0f  ;
	static bool first_frame = true;
	renderer::bind(particle_eff);
	glUniform1f(particle_eff.get_uniform_location("delta_time"), delta_time);	
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_L))
	{
		if (lightningToggle )
		{
			
			light.set_light_colour(vec4(0, 0, 0, 1));
			light.set_ambient_intensity(vec4(0, 0, 0, 1));
			lightningToggle = false;
		}
		else 
		{
			
			light.set_light_colour(vec4(1, 1, 1, 1));
			light.set_ambient_intensity(vec4(0.3, 0.3, 0.3, 1));
			lightningToggle = true;
		}

	}
	cout << 1 / delta_time << endl;
	// Rotate the lights

	velocity += delta_time;
	

	// cell shading
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_T))
	{
		if (cell == 0)
			cell = 1;
		else
			cell = 0;  
	}
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_E))
	{
		normal = !normal;
	}
	update_cameras(delta_time);
	return true;
}
bool render() {

	if (normal)
	{
		renderSkybox(cams[cameraIndex]);
		renderMain(cams[cameraIndex]);

		renderParallax(cams[cameraIndex]);
		renderParticles(cams[cameraIndex]);
		renderHierarchy(cams[cameraIndex]);
		renderPortals(cams[2], cams[cameraIndex], 1);
		renderPortals(cams[3], cams[cameraIndex], 0);
	}
	else
		renderEdge(cams[cameraIndex]);

	return true;
}
void main() {
  // Create application
  app application("Graphics Coursework");
  // Set load content, update and render methods
  application.set_initialise(initialise);
  application.set_load_content(load_content);
  application.set_update(update);
  application.set_render(render);
  // Run application
  application.run();
  delete cams[0];
  delete cams[1];
  delete cams[2];
  delete cams[3];
} 
 