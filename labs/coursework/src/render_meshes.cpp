#include <glm\glm.hpp>
#include <graphics_framework.h>
#include "render_meshes.h"
#include "main.h"

using namespace std;
using namespace std::chrono;
using namespace graphics_framework;
using namespace glm;

void renderSkybox(camera* cam)
{
	glDisable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);
	glCullFace(GL_FRONT);

	// Bind skybox effect
	renderer::bind(sky_eff);
	// Calculate MVP for the skybox
	auto M = skybox.get_transform().get_transform_matrix();
	auto V = cam->get_view();
	auto P = cam->get_projection();
	auto MVP = P*V*M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(sky_eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
	// Set cubemap uniform
	renderer::bind(cube_map, 0);
	glUniform1i(sky_eff.get_uniform_location("cube_map"), 0);

	// Render skybox
	renderer::render(skybox);
	// Enable depth test,depth mask,face culling
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_TRUE);
	glCullFace(GL_BACK);
}
void renderHierarchy(camera* cam)
{
	renderHierarchyOptimisation();
	const auto PV = cam->get_projection() * cam->get_view();
	// Create MVP matrix
	const auto loc = eff.get_uniform_location("MVP");
	// Set MVP matrix uniform
	for (int i = 0; i < hierarchy_meshes.size(); i++)

	{
		auto M = hierarchy_meshes[i].get_transform().get_transform_matrix();
		for (int j = i; j > 0; j--) {
			M = hierarchy_meshes[j - 1].get_transform().get_transform_matrix() * M;
		}

		// Set MVP matrix uniform
		glUniformMatrix4fv(loc, 1, GL_FALSE, value_ptr(PV * M));
		// Bind texture to renderer

		glUniform3fv(eff.get_uniform_location("eye_pos"), 1, value_ptr(cam->get_position()));

		// Render mesh

		renderer::bind(hierarchy_meshes[i].get_material(), "mat");
		glUniformMatrix3fv(eff.get_uniform_location("N"), 1, GL_FALSE, value_ptr(hierarchy_meshes[i].get_transform().get_normal_matrix()));
		glUniformMatrix4fv(eff.get_uniform_location("M"), 1, GL_FALSE, value_ptr(M));

		renderer::render(hierarchy_meshes[i]);
	}


}
void renderParallax(camera* cam)
{
	renderParallaxOptimisation();
	for (auto &e : parallax_meshes) {
		auto m = e.second;
		// Bind effect
		tex = texs[e.first];
		if (normal_maps.find(e.first) != normal_maps.end())
		{
			normal_map = normal_maps[e.first];
		}
		else
		{
			normal_map = normal_maps["basic"];
		}
		// Create MVP matrix
		auto M = m.get_transform().get_transform_matrix();
		auto V = cam->get_view();
		auto P = cam->get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(parallax_eff.get_uniform_location("MVP"), // Location of uniform
			1,                               // Number of values - 1 mat4
			GL_FALSE,                        // Transpose the matrix?
			value_ptr(MVP));                 // Pointer to matrix data
											 // *********************************
		glUniformMatrix4fv(parallax_eff.get_uniform_location("M"), 1, GL_FALSE, value_ptr(M));
	
		// Set N matrix uniform - remember - 3x3 matrix
		//glUniformMatrix3fv(parallax_eff.get_uniform_location("N"), 1, GL_FALSE, value_ptr(m.get_transform().get_normal_matrix()));
		// Bind material
		renderer::bind(m.get_material(), "mat");
		renderer::bind(tex, 0);
		// Set tex uniform   
		glUniform1i(parallax_eff.get_uniform_location("tex"), 0);
		renderer::bind(normal_map, 1);
		// Set normal_map uniform
		glUniform1i(parallax_eff.get_uniform_location("normal_map"), 1);

		// Set eye position - Get this from active camera
		glUniform3fv(parallax_eff.get_uniform_location("viewPos"), 1, value_ptr(cam->get_position()));
		renderer::render(m);

	}
}
void renderMain(camera* cam)
{
	renderMainOptimisation();
	for (auto &e : meshes) {
		auto m = e.second;
		if (e.first == "diskBlue" || e.first == "diskOrange") //|| e.first == "diskOrange")
		{
			continue;
		}
		// Bind effect
		tex = texs[e.first];
		if (normal_maps.find(e.first) != normal_maps.end())
		{
			normal_map = normal_maps[e.first];
		}
		else
		{
			normal_map = normal_maps["basic"];
		}
		
		// Create MVP matrix
		auto M = m.get_transform().get_transform_matrix();
		auto V = cam->get_view();
		auto P = cam->get_projection();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(eff.get_uniform_location("MVP"), // Location of uniform
			1,                               // Number of values - 1 mat4
			GL_FALSE,                        // Transpose the matrix?
			value_ptr(MVP));                 // Pointer to matrix data
											 // *********************************
		glUniformMatrix4fv(eff.get_uniform_location("M"), 1, GL_FALSE, value_ptr(M));
		// Set N matrix uniform - remember - 3x3 matrix
		glUniformMatrix3fv(eff.get_uniform_location("N"), 1, GL_FALSE, value_ptr(m.get_transform().get_normal_matrix()));
		// Bind material
		renderer::bind(m.get_material(), "mat");
		// Bind light

		renderer::bind(tex, 0);
		// Set tex uniform
		glUniform1i(eff.get_uniform_location("tex"), 0);
		renderer::bind(normal_map, 1);
		// Set normal_map uniform
		glUniform1i(eff.get_uniform_location("normal_map"), 1);
		// Set eye position - Get this from active camera
		glUniform3fv(eff.get_uniform_location("eye_pos"), 1, value_ptr(cam->get_position()));

		// Render mesh
		renderer::render(m);

	}
}
void render_feeback()
{
	static bool first_frame = true;
	renderer::bind(particle_eff);

	// Close down the output
	glEnable(GL_RASTERIZER_DISCARD);

	glBindBuffer(GL_ARRAY_BUFFER, particle_buffers_vbo[front_buf]);
	{
		// unfortunatly we can't save this stuff in the VAO. Because OGL.
		GLint position_in_loc = glGetAttribLocation(particle_eff.get_program(), "position_in");
		GLint velocity_in_loc = glGetAttribLocation(particle_eff.get_program(), "velocity_in");
		glEnableVertexAttribArray(position_in_loc);
		glEnableVertexAttribArray(velocity_in_loc);
		glVertexAttribPointer(position_in_loc, 3, GL_FLOAT, GL_FALSE, sizeof(particle), (const GLvoid *)0);
		glVertexAttribPointer(velocity_in_loc, 3, GL_FLOAT, GL_FALSE, sizeof(particle), (const GLvoid *)12);
	}

	// Tell openGL that we are rendering to feedback
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transform_feedbacks[front_buf]);
	glBeginTransformFeedback(GL_POINTS);

	if (first_frame) {
		// compute our points from teh VBO like normal
		glDrawArrays(GL_POINTS, 0, MAX_PARTICLES);
		first_frame = false;
	}
	else {
		// compute from feedback data
		glDrawTransformFeedback(GL_POINTS, transform_feedbacks[back_buf]);
	}
	// Note: the above doesn't actually render, it saves data into either vbo 0/1.

	// stop feedback recording.
	glEndTransformFeedback();

	// reenable output
	glDisable(GL_RASTERIZER_DISCARD);
}
void renderParticles(camera* cam)
{
	render_feeback();
	// Bind the effect
	renderer::bind(eff);
	// Set the MVP matrix
	auto M = mat4(1.0f);
	auto V = cam->get_view();
	auto P = cam->get_projection();
	auto MVP = P * V * M;
	glUniformMatrix4fv(eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
	// Set the colour uniform
	glUniform4fv(eff.get_uniform_location("colour"), 1, value_ptr(vec4(0, 1, 1, 1.0f)));

	// Bind the back particle buffer for rendering
	glBindBuffer(GL_ARRAY_BUFFER, particle_buffers_vbo[front_buf]);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(particle), 0);

	// Perform the render by drawing the transform feedback
	glDrawTransformFeedback(GL_POINTS, transform_feedbacks[front_buf]);
	// Disable vertex attribute array
	glDisableVertexAttribArray(0);

	// *********************************
	// Swap front and back buffers
	glBindBuffer(GL_ARRAY_BUFFER, particle_buffers_vbo[back_buf]);
	front_buf = back_buf;
	back_buf = (back_buf + 1) % 2;
}
void renderPortals(camera* cam, camera* cam2, int nr)
{
	mesh m;
	if (nr == 1)
		m = meshes["diskBlue"];
	else
		m = meshes["diskOrange"];
	//tex = texs["wood"];
	// Set render target to frame buffer
	renderer::set_render_target(frame);
	// Set clear colour to white
	renderer::setClearColour(1, 1, 1);
	// Clear frame
	renderer::clear();
	// *********************************
	// Render meshes
	renderSkybox(cam);
	renderHierarchy(cam);
	renderMain(cam);
	renderParallax(cam);
	renderParticles(cam);

	// Return clear colour to cyan
	renderer::setClearColour(0.0f, 1.0f, 1.0f);
	// *********************************
	// Set render target back to the screen  
	renderer::set_render_target();
	// bind the tex effect
	renderer::bind(tex_eff);
	// Get M from render_cube
	auto M = m.get_transform().get_transform_matrix();
	// get V and P from Cam 2
	auto V = cam2->get_view();
	auto P = cam2->get_projection();
	// Build MVP
	auto MVP = P * V * M;
	// Set MVP matrix uniform
	glUniformMatrix4fv(tex_eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
	// Bind texture from frame buffer
	renderer::bind(frame.get_frame(), 0);
	// Set the tex uniform
	glUniform1i(tex_eff.get_uniform_location("tex"), 0);
	// Render the render cube

	renderer::render(m);
	// *********************************
}
void renderEdge(camera* cam)
{
	// Start rendering to a buffer -----------
	renderer::set_render_target(frameVision);
	renderer::clear();

	renderSkybox(cam);
	renderMain(cam);
	renderHierarchy(cam);
	renderParallax(cam);
	// Start edge detection rendering ------------------------------
	renderer::set_render_target(frameDistortion);  //
	renderer::clear();
	renderer::bind(edge_eff);
	// Bind texture from frame buffer     
	auto MVP = mat4(1);
	// Set MVP matrix uniform
	glUniformMatrix4fv(edge_eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));

	renderer::bind(frameVision.get_frame(), 0);
	// Set the tex uniform
	glUniform1i(edge_eff.get_uniform_location("tex"), 0);

	renderer::render(screen_quad);
	// Start distortion rendering ---------------------------

	renderer::set_render_target(frame);
	renderer::clear();
	renderer::bind(distortion_eff);
	// Set MVP matrix uniform
	glUniformMatrix4fv(distortion_eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));

	renderer::bind(dudv_map, 1);
	glUniform1i(distortion_eff.get_uniform_location("dudvMap"), 1);

	glUniform1f(distortion_eff.get_uniform_location("moveFactor"), moveFactor);

	renderer::bind(frameDistortion.get_frame(), 0);
	// Set the tex uniform
	glUniform1i(distortion_eff.get_uniform_location("tex"), 0);

	renderer::render(screen_quad);

	//start mask rendering ---------------------------
	// Set render target back to the screen
	renderer::set_render_target();
	renderer::clear();
	// Bind Tex effect
	renderer::bind(mask_eff);
	// Set MVP matrix uniform
	glUniformMatrix4fv(mask_eff.get_uniform_location("MVP"), 1, GL_FALSE, value_ptr(MVP));
	// Bind texture from frame buffer to TU 0
	renderer::bind(frame.get_frame(), 0);
	// Set the tex uniform, 0
	glUniform1i(mask_eff.get_uniform_location("tex"), 0);
	// Bind alpha texture to TU, 1
	renderer::bind(alpha_map, 1);
	// Set the tex uniform, 1
	glUniform1i(mask_eff.get_uniform_location("alpha_map"), 1);
	// Render the screen quad
	renderer::render(screen_quad);
}
void renderParallaxOptimisation()
{
	depth_map = depth_maps["plane1"];
	renderer::bind(parallax_eff);
	glUniform1i(parallax_eff.get_uniform_location("cell"), cell);
	// Bind light
	renderer::bind(spots, "spots");
	// Bind texture 
	renderer::bind(light, "light");
	// ---
	glUniform1f(parallax_eff.get_uniform_location("height_scale"), height_scale);

	glUniform3fv(parallax_eff.get_uniform_location("lightPos"), 1, value_ptr(spots[3].get_position()));
	renderer::bind(depth_map, 2);
	// Set normal_map uniform
	glUniform1i(parallax_eff.get_uniform_location("depth_map"), 2);
}
void renderHierarchyOptimisation()
{
	normal_map = normal_maps["torusBlue"];
	tex = texs["white"];
	renderer::bind(eff);
	renderer::bind(tex, 0);
	renderer::bind(normal_map, 1);
	renderer::bind(spots, "spots");
	// Bind texture
	glUniform1i(eff.get_uniform_location("tex"), 0);
	glUniform1i(eff.get_uniform_location("normal_map"), 1);
	renderer::bind(light, "light");
}
void renderMainOptimisation()
{
	renderer::bind(eff);
	renderer::bind(spots, "spots");
	// Bind texture
	renderer::bind(light, "light");
	glUniform1i(eff.get_uniform_location("cell"), cell);
} 