#version 440

// Spot light data
struct spot_light {
  vec4 light_colour;
  vec3 position;
  vec3 direction;
  float constant;
  float linear;
  float quadratic;
  float power;
};

// Material data
struct material {
  vec4 emissive;
  vec4 diffuse_reflection;
  vec4 specular_reflection;
  float shininess;
};

// Spot light being used in the scene
uniform spot_light spot;
// Material of the object being rendered
uniform material mat;
// Position of the eye (camera)
uniform vec3 eye_pos;
// Texture to sample from
uniform sampler2D tex;

// Incoming position
layout(location = 0) in vec3 position;
// Incoming normal
layout(location = 1) in vec3 normal;
// Incoming texture coordinate
layout(location = 2) in vec2 tex_coord;

// Outgoing colour
layout(location = 0) out vec4 colour;
float step (float edge , float x)
{
	return x<edge ? 0.0 : 1.0;
}
float smoothstep (float edge0,float edge1,float x)
{
	if (x<=edge0)
	return 0;
	if(x>= edge1)
	return 1.0;

	float t  = clamp((x-edge0) /(edge1-edge0),0,1); 
	return t*t*(3-2*t);
}

float mixF (float a, float b , float t)
{
	return a*(1.0 - t) + b * t;
}
void main() {
	const float A = 0.1;
	const float B = 0.3;
	const float C = 0.6;
	const float D = 1.0;
  // *********************************
  // Calculate direction to the light
   vec3 L = normalize(spot.position - position);
  // Calculate distance to light
  float d = distance(spot.position,position);
  // Calculate attenuation value
  float att = spot.constant + spot.linear *d +  spot.quadratic*d*d;
  // Calculate spot light intensity
  // Calculate light colour
  vec4 light_colour = spot.light_colour *(pow(max(dot((-1)*spot.direction,L),0.0f),spot.power))/ att;
  // Calculate view direction

  // Now use standard phong shading but using calculated light colour and direction
  // - note no ambient
  float df = max(dot(normal,L),0.0f);

  float E = fwidth(df);

  if (df > A - E && df < A + E)
	df = mix(A, B, smoothstep(A - E, A + E, df));
  else if (df > B - E && df < B + E)
	df = mix(B, C, smoothstep(B - E, B + E, df));
  else if (df > C - E && df < C + E)
   df = mix(C, D, smoothstep(C - E, C + E, df));
  else if(df<A) 
	df =0.0f;
	else if (df<B)
	df = B;
	else if(df<C)
    df = C;
	else 
	df = D;
  vec4 diffuse = df *(mat.diffuse_reflection*light_colour);
  vec3 view_dir = normalize (eye_pos-position);
  vec3 half_vector = normalize(L + view_dir);
 float sf = pow(max(dot(normal,half_vector),0),mat.shininess);

 E = fwidth(sf) ; 

 if(sf > 0.5 -E && sf< 0.5 + E)
 {
	sf = clamp(0.5 * (sf - 0.5 + E)/E , 0.0,1.0);
 }
 else
	sf = step(0.5,sf);


  vec4 specular =  sf* (mat.specular_reflection*light_colour);
  vec4 tex_colour = texture(tex,tex_coord);
  vec4 primary = mat.emissive + diffuse;
    primary.a = 1;
  colour = primary*tex_colour +  specular;
  // *********************************
}