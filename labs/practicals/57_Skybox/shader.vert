#version 440

// The transformation matrix
uniform mat4 MVP;

// Incoming position
layout (location = 0) in vec3 position;

layout (location = 0) out vec3 position_out;
void main()
{
	// Transform the position into screen space
	gl_Position = MVP * vec4(position, 1.0);
	position_out = position;
}