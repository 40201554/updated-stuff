#version 440
#extension GL_NV_shadow_samplers_cube : enable
uniform samplerCube cube_map;
layout (location = 0) in vec3 position;
layout (location = 0) out vec4 colour;

void main()
{
	vec4 tex_colour = textureCube(cube_map,position);
	colour = tex_colour;
}